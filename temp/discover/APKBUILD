# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=discover
pkgver=5.20.90
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# s390x blocked by flatpak
arch="all !armhf !s390x !mips64"
url="https://userbase.kde.org/Discover"
pkgdesc="KDE Plasma resources management GUI"
license="LGPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
depends="kirigami2"
makedepends="
	appstream-dev
	attica-dev
	extra-cmake-modules
	flatpak-dev
	karchive-dev
	kauth-dev
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	kitemmodels-dev
	knewstuff-dev
	kxmlgui-dev
	libapk-qt-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/discover-$pkgver.tar.xz
	0001-Add-support-for-Alpine-Linux-apk-backend.patch
	alpine-appstream-data.json
	"
subpackages="
	$pkgname-lang
	$pkgname-backend-flatpak:backend_flatpak
	$pkgname-backend-apk:backend_apk
	"

case "$CARCH" in
	x86|x86_64)
		makedepends="$makedepends fwupd-dev"
		subpackages="$subpackages $pkgname-backend-fwupd:backend_fwupd"
		;;
	*) ;;
esac

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_DISABLE_FIND_PACKAGE_Snapd=ON
	cmake --build build
}

check() {
	cd build
	# knsbackendtest and flatpaktest fail to find their required executables
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(knsbackend|flatpak)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

backend_flatpak() {
	pkgdesc="Flatpak backend for $pkgname"
	depends=""
	install_if="$pkgname flatpak"

	mkdir -p \
		"$subpkgdir"/usr/lib/qt5/plugins/discover \
		"$subpkgdir"/usr/share/libdiscover/categories \
		"$subpkgdir"/usr/lib/qt5/plugins/discover-notifier \
		"$subpkgdir"/usr/share/applications \
		"$subpkgdir"/usr/share/metainfo \
		"$subpkgdir"/usr/share/icons/hicolor/scalable/apps

	mv "$pkgdir"/usr/lib/qt5/plugins/discover/flatpak-backend.so \
		"$subpkgdir"/usr/lib/qt5/plugins/discover/
	mv "$pkgdir"/usr/share/libdiscover/categories/flatpak-backend-categories.xml \
		"$subpkgdir"/usr/share/libdiscover/categories/
	mv "$pkgdir"/usr/lib/qt5/plugins/discover-notifier/FlatpakNotifier.so \
		"$subpkgdir"/usr/lib/qt5/plugins/discover-notifier/
	mv "$pkgdir"/usr/share/applications/org.kde.discover-flatpak.desktop \
		"$subpkgdir"/usr/share/applications/
	mv "$pkgdir"/usr/share/metainfo/org.kde.discover.flatpak.appdata.xml \
		"$subpkgdir"/usr/share/metainfo/
	mv "$pkgdir"/usr/share/icons/hicolor/scalable/apps/flatpak-discover.svg \
		"$subpkgdir"/usr/share/icons/hicolor/scalable/apps/
}

backend_apk() {
	pkgdesc="Apk backend for $pkgname"
	depends=""
	install_if="$pkgname apk-tools"

	mkdir -p \
		"$subpkgdir"/usr/lib/qt5/plugins/discover \
		"$subpkgdir"/usr/share/libdiscover/external-appstream-urls \
		"$subpkgdir"/usr/lib/libexec/kauth/ \
		"$subpkgdir"/usr/share/polkit-1/actions/ \
		"$subpkgdir"/usr/share/dbus-1/system.d/ \
		"$subpkgdir"/usr/share/dbus-1/system-services/

	mv "$pkgdir"/usr/lib/qt5/plugins/discover/alpineapk-backend.so \
		"$subpkgdir"/usr/lib/qt5/plugins/discover/
	mv "$pkgdir"/usr/lib/libexec/kauth/alpineapk_kauth_helper \
		"$subpkgdir"/usr/lib/libexec/kauth/
	mv "$pkgdir"/usr/share/polkit-1/actions/org.kde.discover.alpineapkbackend.policy \
		"$subpkgdir"/usr/share/polkit-1/actions/
	mv "$pkgdir"/usr/share/dbus-1/system.d/org.kde.discover.alpineapkbackend.conf \
		"$subpkgdir"/usr/share/dbus-1/system.d/
	mv "$pkgdir"/usr/share/dbus-1/system-services/org.kde.discover.alpineapkbackend.service \
		"$subpkgdir"/usr/share/dbus-1/system-services/

	# JSON file contains placeholder for replacement - @CARCH@
	sed -i "s/@CARCH@/$CARCH/g" "$srcdir"/alpine-appstream-data.json
	install -Dm644 "$srcdir"/alpine-appstream-data.json \
		"$subpkgdir"/usr/share/libdiscover/external-appstream-urls/alpine-appstream-data.json
}

backend_fwupd() {
	pkgdesc="fwupd backend for $pkgname"
	depends=""
	install_if="$pkgname fwupd"

	mkdir -p \
		"$subpkgdir"/usr/lib/qt5/plugins/discover

	mv "$pkgdir"/usr/lib/qt5/plugins/discover/fwupd-backend.so \
		"$subpkgdir"/usr/lib/qt5/plugins/discover/
}

sha512sums="f5cde987e5026eece047a32285421bf1eada4ed9496c514378c16ccd99dcfe768fc5d64caf63bbcad53295d60e21c567fd6712a09006f0adbcb66f809413ac9e  discover-5.20.90.tar.xz
11240b3a8b461acc73e76fee37790e2f7c48dcb034dca8b25dc3728f97259736f2475dbd141a9fa1d7dbe44453bfcafa93178156f21db5010a3d3cd9a7c6ec73  0001-Add-support-for-Alpine-Linux-apk-backend.patch
f9f73888f8e20b317987da55028bd578de854fb34293466d1bc5deb82e32dc164916f724411de64a42235ecda37205d3f1ba26621ed7ab710b94273acce34aa3  alpine-appstream-data.json"
