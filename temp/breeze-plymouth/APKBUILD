# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze-plymouth
pkgver=5.20.90
pkgrel=0
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/"
pkgdesc="Breeze theme for Plymouth"
license="GPL-2.0-or-later"
depends="plymouth"
makedepends="
	extra-cmake-modules
	plymouth-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-plymouth-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DDISTRO_NAME="$(grep PRETTY_NAME /etc/os-release | awk -F  = '{ print $2 }')" \
		-DDISTRO_VERSION="$(grep VERSION_ID /etc/os-release | awk -F  = '{ print $2 }')"
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="e2e50afe6ff4c86cb5b1ed02f099183eff9ff41f8ec6788470413b392b340add12d5b9a614702ea2a8d23cf6675472fb1efc4bbcfedd4773725eda0308ffcb57  breeze-plymouth-5.20.90.tar.xz"
