# Forked from Alpine to package newer commits
pkgname=plasma-phone-components
pkgver=5.20.90
pkgrel=0
pkgdesc="Modules providing phone functionality for Plasma"
arch="all !armhf !x86" # x86 blocked by maliit-keyboard
url="https://www.plasma-mobile.org/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="
	breeze-icons
	dbus-x11
	kactivities
	libqofono
	maliit-keyboard
	plasma-nano
	plasma-nm
	plasma-pa
	plasma-settings
	plasma-workspace
	qqc2-breeze-style
	qt5-qtquickcontrols2
	telepathy-ofono
	"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kauth-dev
	kbookmarks-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kitemviews-dev
	kjobwidgets-dev
	kpackage-dev
	kservice-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	solid-dev
	"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-phone-components-$pkgver.tar.xz
	set-postmarketos-wallpaper.patch
	0001-add-back-panels-if-they-disappeared.patch
	0002-fix-logic-error.patch
	0003-Use-loop-instead-of-recursion.patch
	"
subpackages="$pkgname-lang"
options="!check" # No tests

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="9fef94b71ed7314e9fb37bb4c083cfa6eb86167507c76c6f8cd0a1413a7d3fbf3eaaa514d10a83bbb475303b0a009e68989ab6de1a3ae63c650e07ff8da07698  plasma-phone-components-5.20.90.tar.xz
aef46ceb3cd4c49fe22b62a9821951bec366291e8ec060c7304d9db9c150a58311f545445c47605afc17dc061466b04bde0abe2d7a3791fa254ef0b7f4ec4e1e  set-postmarketos-wallpaper.patch
30689399c419c8443d5e119679020254507bb4f520bcc3f0730e797612bbee5580b2f012682791f5563998f3c796798d572e03766a3104294b103625873034cb  0001-add-back-panels-if-they-disappeared.patch
101d0e545f17c68a661fabcd0882a77cb8552aed7370c287c2069e2bc76ddf0f642a3996f9438f7f1571f761ec0644ca25229f016bf280bfacbd68fce33559be  0002-fix-logic-error.patch
f838268ad0919e5ea0041428b94c09d4a91e2a264f762889e0bd8a9d3f4673fc7e931c579185f0030216cb6b3520a1714e7336960e634e48827a216b189a517c  0003-Use-loop-instead-of-recursion.patch"
