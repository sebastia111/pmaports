# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kgamma5
pkgver=5.20.90
pkgrel=0
pkgdesc="Adjust your monitor's gamma settings"
# armhf blocked by extra-cmake-modules
# s390x blocked by kconfigwidgets
arch="all !armhf !s390x !mips64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kdoctools-dev
	ki18n-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kgamma5-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="e958edb06b22d0acfae6d4184d955111435ac35deced22590574f274f5048dc83c3bf05fb6c9ecec69b31730e0f5aab94e7b9cce5d05ceacd034e58e044b00d0  kgamma5-5.20.90.tar.xz"
