# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=powerdevil
pkgver=5.20.90
pkgrel=0
pkgdesc="Manages the power consumption settings of a Plasma Shell"
# armhf blocked by extra-cmake-modules
# s390x blocked by libksysguard
arch="all !armhf !s390x !mips64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="upower"
depends_dev="
	bluez-qt-dev
	eudev-dev
	kactivities-dev
	kauth-dev
	kconfig-dev
	kcrash-dev
	kdbusaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	knotifications-dev
	knotifyconfig-dev
	kwayland-dev
	libkscreen-dev
	networkmanager-qt-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	solid-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/powerdevil-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="b8db4b703b5e0827ec78167bbffaa04b5be4945f3d0c15d06ed8552d8b1a4ea792ce43f18b9d3bf9288f365a61d7d41fac4b8dbf482107cfbfb76e3b7ccc7eac  powerdevil-5.20.90.tar.xz"
