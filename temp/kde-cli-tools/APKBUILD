# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-cli-tools
pkgver=5.20.90
pkgrel=0
pkgdesc="Tools based on KDE Frameworks 5 to better interact with the system"
# armhf blocked by extra-cmake-modules
# s390x blocked by libksysguard
arch="all !armhf !s390x !mips64"
url="https://invent.kde.org/plasma/kde-cli-tools"
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0-or-later AND GPL-2.0-only AND LGPL-2.1-only"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kcmutils-dev
	kconfig-dev
	kdeclarative-dev
	kdesu-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kservice-dev
	kwindowsystem-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kde-cli-tools-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

# Workaround a circular dependency https://gitlab.alpinelinux.org/alpine/aports/-/issues/11785
install_if="plasma-workspace"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="dcf18a4537c4d8592d952d634be4a18c1c43a37d146b42d344acc82500b321b9886a9cc6e9916a079b2c2718f79e503a89cc1aadb0d0d7f6d0384fd71fcdf774  kde-cli-tools-5.20.90.tar.xz"
